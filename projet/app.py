from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
import os.path
from flask_bootstrap import Bootstrap

app = Flask( __name__ )
app.config['BOOTSTRAP_SERVE_LOCAL'] = True
Bootstrap (app)

def mkpath(p):
    return os.path.normpath(os.path.join(os.path.dirname(__file__),p))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = ('sqlite:///'+mkpath('bd.db'))
login_manager = LoginManager(app)
login_manager.login_view = "login"
app.config['SECRET_KEY'] = "7E9B2040-FFA0-4383-9BA9-4AC714F0F10B"
db = SQLAlchemy(app)
