from .app import app, db
import projet.models
import projet.views

from .models import Artist, Album
import yaml

db.create_all()
with open('extrait.yaml') as f:
    albums = yaml.load(f, Loader=yaml.FullLoader)

artists ={}
for albu in albums:
    a = albu['by']
    if a not in artists:
        o = Artist(name=a)
        db.session.add(o)
        artists[a]=o
    db.session.commit()

for albu in albums:
    a = artists[albu['by']]
    o = Album(img = albu["img"],
            title = albu["title"],
            releaseYear = albu["releaseYear"],
            artist_id = a.id,
            artist_name = a.name,
            parent = albu["parent"],
	url= albu["url"]
            )
    db.session.add(o)

db.session.commit()
