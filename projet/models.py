import yaml, os.path
from .app import db, login_manager
from flask_login import UserMixin

class Artist(db.Model):
    id = db.Column(db.Integer, autoincrement=True, primary_key =True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return "<Artist %s>" % (self.name)


class Album(db.Model):
    id = db.Column(db.Integer, primary_key =True)
    img = db.Column(db.String(100))
    title = db.Column(db.String(100))
    releaseYear = db.Column(db.Integer)
    artist_name = db.Column(db.Integer)
    parent = db.Column(db.String(100))
    artist_id = db.Column(db.Integer, db.ForeignKey("artist.id"))
    url = db.Column(db.String(100))
    artist = db.relationship("Artist",backref=db.backref("albums", lazy="dynamic"))

    def __repr__(self):
        return "<Album (%d) %s>" % (self.id, self.title)

def get_sample():
    return Album.query.limit(8).all()


def get_all_albums():
    return Album.query.limit(21).all()


def get_album(id):
    return Album.query.get_or_404(id)

def get_all_artists():
    return Artist.query.limit(14).all()

def get_artist(id):
    return Artist.query.get_or_404(id)

def get_album_by_artist(id):
    return Album.query.filter_by(artist_id=id)

from flask_login import UserMixin

class User(db.Model, UserMixin):
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))

    def get_id(self):
        return self.username

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)
