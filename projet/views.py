from .app import app, db
from flask import render_template, url_for, redirect, flash, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import login_user, current_user, logout_user, login_required


class LoginForm(FlaskForm):
    username = StringField('Pseudo')
    password = PasswordField('Mot de passe')
    next = HiddenField()

    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


@app.route("/")
def home():
    return render_template(
        "home.html",
        albums=get_sample()
    )

@app.route("/albums/")
def all_albums():
    return render_template(
        "albums.html",
        title="Tous les albums",
        albums=get_all_albums()
    )

@app.route("/albums/<int:id>")
def one_album(id):
    album=get_album(id)
    return render_template(
        "album.html",
        title=album.title,
        album=get_album(id)
    )


@app.route("/artists/")
def all_artists():
    return render_template(
        "artists.html",
        title="Tous les artistes",
        artists=get_all_artists()
    )



@app.route("/artist/<int:id>")
def one_artist(id):
    artist=get_artist(id)
    return render_template(
        "artist.html",
        title=artist.name,
        artist=get_artist(id),
        albums=get_album_by_artist(id)
    )

@app.route("/edit/artist/")
@app.route("/edit/artist/<int:id>")
@login_required
def edit_artist(id = None):
    nom = None
    if id is not None:
        a = get_artist(id)
        nom = a.name
    else:
        a = None
    f = ArtistForm(id = id, name = nom)
    return render_template("edit-artist.html", artist = a, form = f)

@app.route("/save/artist/", methods=["POST"])
def save_artist():
    a = None
    f = ArtistForm()
    if f.validate_on_submit():
        if not get_artist_by_name(f.name.data):
            if f.id.data != "":
                id = int(f.id.data)
                a = get_artist(id)
                a.name = f.name.data
            else:
                a = Artist(name = f.name.data)
                db.session.add(a)
            db.session.commit()
            id = a.id
            return redirect(url_for('one_artist', id = id))
        else:
            flash("Authorallready exists")
    return render_template("edit-artist.html", artist = a, form = f)

@app.route("/login/", methods = ["GET", "POST"])
def login():
    f = LoginForm()
    if not f.is_submitted():
        f.next.data = request.args.get("next")
    elif f.validate_on_submit():
        user = f.get_authenticated_user()
        if user:
            login_user(user)
            next = f.next.data or (url_for('home'))
            return redirect(next)
    return render_template("login.html", form = f)

@app.route('/logout/')
def logout():
    logout_user()
    return redirect(url_for('home'))
