import click
from .app import *

@app.cli.command()
@click.argument('username')
@click.argument('password')
def newuser(username, password):
    m = sha256()
    m.update(password.encode())
    u = User(username = username, password = m.hexdigest())
    db.session.add(u)
    db.session.commit()

@app.cli.command()
@click.argument('username')
@click.argument('password')
def passwd(username, password):
    m = sha256()
    m.update(password.encode())
    u = User.query.get(username)
    if u:
        u.password = m.hexdigest()
        db.session.commit()
    else:
        print("****************************************************")
        print("Il n'y a pas de compte associé au pseudo " + username)
        print("****************************************************")
